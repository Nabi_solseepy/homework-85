import React, {Component, Fragment} from 'react';
import {Button, Card, CardBody, CardColumns} from "reactstrap";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {delteAlbums, fetchAlbums, publishedAlbum} from "../../store/actions";
import AlbumsThumbinail from "../../components/AlbumsThumbinail/AlbumsThumbinail";

class Albums extends Component {
    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.fetchAlbums(id);
    }

    render() {
        return (
            <Fragment>
                {/*{this.props.albums ? <h2>{this.props.albums[0].artist.name}</h2>: null}*/}
                <CardColumns>

                    {this.props.albums && this.props.albums.length > 0 ? (
                        this.props.albums.map(album => (
                            <Card key={album._id}>
                                <AlbumsThumbinail image={album.image}/>

                                <Link to={`/${album.name}/${this.props.match.params.name}/${album._id}/tracks`}>{album.name}</Link>
                                <CardBody>
                                    {album.year}

                                    {this.props.user && this.props.user.role === 'admin' && (
                                        <Fragment>
                                            {album.published === false  ? (
                                                <p>
                                                    <p>неопубликовано</p>
                                                    <Button  onClick={() => this.props.publishedAlbum(album._id)} color="primary">Опубликовать</Button>
                                                </p>
                                            ): null}
                                            <Button onClick={() => this.props.delteAlbums(album._id, album.artist._id)} color="danger">Удалить</Button>
                                        </Fragment>
                                    )}
                                </CardBody>

                            </Card>
                        ))
                    ): <div>Not albums published</div>}

                </CardColumns>
            </Fragment>

        );
    }
}

const mapStateToProps = state => ({
   albums: state.reducer.albums,
   user: state.user.user
});


const mapDispatchToProps = dispatch => ({
    fetchAlbums:(id) => dispatch(fetchAlbums(id)),
    publishedAlbum: (id) => dispatch(publishedAlbum(id)),
    delteAlbums: (albumId, artistId) => dispatch(delteAlbums(albumId, artistId))
});

export default connect(mapStateToProps,mapDispatchToProps)(Albums);