import axios from '../axios-api';
import {push} from 'connected-react-router';

export const GET_TRACK_HISTORY_SUCCESS = 'GET_TRACK_HISTORY_SUCCESS';
export const SEND_TRACK_HISTORY_SUCCESS = 'SEND_TRACK_HISTORY_SUCCESS';

export const getTrackHistorySuccess = (trackHistory) => ({type: GET_TRACK_HISTORY_SUCCESS, trackHistory})
export const sendTrackHistorySuccess = () => ({type: SEND_TRACK_HISTORY_SUCCESS});

export const getTrackHistory = () => {
    return (dispatch, getState) => {
        const user = getState().user.user;
        if (!user) {
            dispatch(push('/login'))
        } else {
            axios.get('/track_history', {headers: {'Authorization': user.token}}).then(
                response => {
                    dispatch(getTrackHistorySuccess(response.data))
                }
            )
        }
    }
};


export const sendTrackHistory = trackId => {
    return (dispatch, getState) => {
        if (!getState().user.user) return null;

        const token = getState().user.user.token;
        return axios.post('/track_history', trackId, {headers: {'Authorization':  token}}).then(response => {
            dispatch(sendTrackHistorySuccess())
        })
    }
};

