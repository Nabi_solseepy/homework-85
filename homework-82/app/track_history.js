const express = require('express');

const TrackHistory = require('../models/TrackHistory');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const User = require('../models/User');
const router = express.Router();

router.get('/', async (req, res) =>{
   const token = req.get('Authorization') ;

    if (!token) {
        return res.status(401).send({error: 'Token not provided'})
    }

    const user = await User.findOne({token});
    if (!user){
        return res.status(401).send({error: 'Token incorrect'})
    }


    TrackHistory.find({user: user._id}).sort({datetime: -1}).populate({path: 'track', populate: {path: 'album', populate: {path: 'artist'}}})
        .then(result => res.send(result))
        .catch(() => res.send(500))
});


router.post('/', async (req, res) =>{
   const token = req.get('Authorization');

    const user = await User.findOne({token});


    if (!user) {
        return res.status(401).send({error: 'User not found'})
    }

    const trackHistory = new TrackHistory(req.body);

    trackHistory.user = user._id;

    trackHistory.datetime = new Date().toISOString();

    trackHistory.save()
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500));
});




module.exports = router;