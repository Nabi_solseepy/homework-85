const mongoose = require('mongoose');


const Schema = mongoose.Schema;

const AlbumSchema = new Schema({
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true
    },
    name: {
        type: String,
        required: true
    },
    artist: {
        type: Schema.Types.ObjectId,
        ref: 'Artist',
        required: true
    },
    delete: {
        type: Boolean,
        required:true,
        default: false
    },
    published: {
        type: Boolean,
        required: true,
        default: false
    },
    year: String,
    image: String
});

const Album = mongoose.model('Album', AlbumSchema );

module.exports = Album;