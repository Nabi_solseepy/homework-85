import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "reactstrap";
import FormElement from "../../components/UI/Form/FromElement";
import {createTrack, fetchAlbums} from "../../store/actions";
import {connect} from "react-redux";

class AddFormTracks extends Component {

    state = {
        name: '' ,
        album: '',
        duration: ''

    };


    componentDidMount() {
        this.props.fetchAlbums()
    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };



    submitFormHandler = event => {
        event.preventDefault();

        this.props.createTrack({...this.state});
    };


    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <FormElement
                    propertyName="album"
                    title="Album"
                    type="select"
                    value={this.state.album}
                    onChange={this.inputChangeHandler}
                >
                    <option value="">Please select a category</option>
                    {this.props.albums &&  this.props.albums.map(album => (
                        <option key={album._id} value={album._id}>{album.name}</option>
                    ))}
                </FormElement>

                <FormElement
                    propertyName="name"
                    title="Name"
                    type="text"
                    value={this.state.name}
                    onChange={this.inputChangeHandler}/>

                <FormElement
                    propertyName="duration"
                    title="Duration"
                    type="text"
                    value={this.state.duration}
                    onChange={this.inputChangeHandler}/>

                <FormGroup row>
                    <Col sm={{offset: 2, size: 10}}>
                        <Button type="submit" color="primary">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = state => ({
    albums: state.reducer.albums
});


const mapDispatchToProps = dispatch => ({
    createTrack: (trackData) => dispatch(createTrack(trackData)),
    fetchAlbums: () => dispatch(fetchAlbums())
});

export default connect(mapStateToProps,mapDispatchToProps)(AddFormTracks);