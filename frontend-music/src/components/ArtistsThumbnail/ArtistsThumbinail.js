import React from 'react';


const styles = {
    width: '100px',
    height: '100px',
    marginRight: '10px'
};
const ArtistsThumbinail = (props) => {
    let image;

    if (props.image) {
        image = `http://localhost:8000/uploads/${props.image}`;
    }

    return <img src={image} style={styles} className="img-thumbnail" alt="product"/>;
};

export default ArtistsThumbinail;