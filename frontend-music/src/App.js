import React, { Component,Fragment} from 'react';
import './App.css';
import {Route, withRouter} from "react-router-dom";
import Toolbar from "./components/UI/Toolbar/Toolbar";
import Artists from "./container/Artists/Artists";
import Container from "reactstrap/es/Container";
import Albums from "./container/Albums/Albums";
import Tracks from "./container/Tracks/Tracks";
import Register from "./container/Register/Register";
import Login from "./container/Login/Login";
import {connect} from "react-redux";
import TrackHistory from "./container/Track_History/Track_History";
import AddAlbum from "./container/AddAlbum/AddAlbum";
import AddFormArtists from "./container/AddFormArtists/AddFormArtists";
import AddFormTracks from "./container/AddFormTracks/AddFormTracks";
import {NotificationContainer} from "react-notifications";
import {logoutUser} from "./store/usersActions";


class App extends Component {
  render() {
    return (
 <Fragment>
     <NotificationContainer/>
   <Toolbar user={this.props.user}
            logout={this.props.logoutUser}
   />
     <Container>
         <Route path="/" exact component={Artists}/>
          <Route path="/:name/:id" exact  component={Albums}/>
           <Route path="/:albumName/:artistName/:id/tracks" exact component={Tracks}/>
            <Route path="/register" exact component={Register} />
            <Route path="/login" exact component={Login} />
         <Route path="/track_history" component={TrackHistory}/>
         <Route path="/addAlbum" exact component={AddAlbum}/>
         <Route path="/addArtists"  exact component={AddFormArtists}/>
         <Route path='/addTracks' exact component={AddFormTracks}/>
     </Container>
 </Fragment>
    );
  }
}

const mapStateTpProps = state => ({
    user: state.user.user
});

const  mapDispatchToProps= dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateTpProps,mapDispatchToProps)(App));
