const express = require('express');
const nanoid = require('nanoid');
const multer = require('multer');
const config =require('../config');
const Artist = require('../models/Artist');
const Album = require('../models/Album');
const Track = require('../models/Track');

const tryAuth = require('../middleware/tryAuth');

const auth = require('../middleware/auth');
const permit  =require('../middleware/permit');

const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, cd) => {
        cd(null , config.uploadPath);
    },
    filename: (req, file, cd) => {
        cd(null , nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();


router.get('/', tryAuth, async (req,res) =>{

    let published = {published: true, delete: false};

    if (req.user && req.user.role === 'admin'){
        published = {delete: false}
    }

    Artist.find(published)
    .then(artists => res.send(artists))
    .catch(() => res.sendStatus(500))
});

router.post('/', auth, upload.single('image'), (req, res) => {
    const artistData = req.body;

    if (req.file) {
        artistData.image = req.file.filename
    }

    const artist = new Artist(artistData);

    artist.user = req.user._id;


    artist.save()
        .then(result => res.send(result))
        .catch(error => res.sendStatus(400).send(error))
});


router.get('/:id/toggle_published', [auth, permit('admin')], async (req, res) => {
    try{
        await Artist.updateOne({_id: req.params.id}, {$set: {published: true}});
        res.status(200).send();
    }catch (e) {
        res.status(400).send(e);
    }
});



router.delete('/delete/:id', [auth, permit('admin')], async (req, res) => {

    try {
        const artist = await Artist.findById(req.params.id);


        await Artist.updateOne({_id: artist._id}, {$set: {delete: true}});


        if (!artist) {
            res.sendStatus(400)
        }

        const album = await Album.find({artist: artist._id});

        await Album.updateMany({artist: artist._id}, {$set: {delete: true}});

        if (album.length <= 0) {
            res.sendStatus(200)
        }

        // const track = await Track.find({album: album._id});

        await Promise.all(album.map(oneAlbum => Track.updateMany({album: oneAlbum._id}, {$set: {delete: true}})));



        res.status(200).send(artist)
    } catch (e) {
        res.send(e)
    }

});



module.exports = router;