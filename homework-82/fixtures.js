const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');

const User = require('./models/User');

const Album  = require('./models/Album');
const Artist = require('./models/Artist');
const Track = require('./models/Track');



const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;
    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop()
    }

   const user = await User.create({
        username: 'user',
        password: '123',
        role: 'user',
        token: nanoid(),
       displayName: 'John Doe'

   },{
        username: 'admin',
        password: '123',
        role: 'admin',
        token: nanoid(),
        displayName: 'Ben Ascren'
    });



    const artist = await Artist.create(
        {
            name: 'macklemore',
            image: 'macklemore.jpg',
            text: 'Бенджамин Хаммонд Хаггерти, известный под псевдонимом Macklemore, является американским рэпером, певцом и автором песен из Сиэтла, штат Вашингтон. Он сотрудничал с продюсером Райаном Льюисом как Macklemore & Ryan Lewis. С 2000 года он самостоятельно выпустил один микстейп, три EP и четыре альбома. ',
            published: true,
            user: user[0]._id

        },
        {
            name: ' the neighbourhood',
            image: 'neighbourhood.jpg',
            text: 'The Neighbourhood - американская рок-группа, созданная в 2011 году в Ньюбери-Парк, штат Калифорния. Группа состоит из вокалиста Джесси Резерфорда, гитаристов Джереми Фридмана и Зака ​​Абелса, басиста Мики Марготта и барабанщика Брэндона Александра Фрида. ',
            user: user[0]._id
        }
    );
    const album = await Album.create(
        {
            name: 'Gemini',
            artist: artist[0]._id,
            year: 'September 22, 2017',
            image: 'Gemini.jpeg',
            user: user[0]._id

        },
        {
            name: 'I Love You',
            artist: artist[1]._id,
            year: ' April 22, 2013',
            image: 'nbh.jpeg',
            user: user[0]._id

        },
        {
            name: 'Wiped Out!',
            artist: artist[1]._id,
            year: 'October 30, 2015',
            image: 'Wiped_Out.jpeg',
            user: user[0]._id

        }
    );
     await  Track.create(
         {name: ' glorious' , album: album[0]._id,duration: '3:35', trackNumber: 1,published: true, user: user[0]._id},
         {name: 'marmalade', album: album[0]._id, duration: '4:24', trackNumber: 2, published: true, user: user[0]._id},
         {name: 'Prey', album: album[2]._id,duration: '4:43', trackNumber: 1, published: false,user: user[0]._id},
         {name: 'Cry Baby', album: album[2]._id, duration: '3:35', trackNumber: 2, published: false, user: user[0]._id},
         {name: 'How', album: album[1]._id,duration: '5:14', trackNumber: 1, published: false, user: user[0]._id},
         {name: 'Afraid', album: album[1]._id, duration:'4:11', trackNumber: 2, published: false, user: user[0]._id}

     );

    await connection.close();
};




run().catch(error => {
    console.log('Something went wrong', error)
});








