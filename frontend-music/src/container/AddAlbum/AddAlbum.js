import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "reactstrap";
import {createAlbum, fetchArtists} from "../../store/actions";
import {connect} from "react-redux";
import FormElement from "../../components/UI/Form/FromElement";

class AddAlbum extends Component {
    state = {
        name: '',
        artist: '',
        year: '',
        image: ''
    };



    componentDidMount() {
         this.props.fetchArtists()

    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };



    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.createAlbum(formData);
    };


    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };
    render() {
        return (
       <Form onSubmit={this.submitFormHandler}>
           <h2>Add Album</h2>
           <FormElement
               propertyName="artist"
               title="Artist"
               type="select"
               value={this.state.artist}
               onChange={this.inputChangeHandler}
           >
               <option value="">Please select a category</option>
               {this.props.artists.map(artist => (
                   <option key={artist._id} value={artist._id}>{artist.name}</option>
               ))}
           </FormElement>
           <FormElement
               propertyName="name"
               title="Name"
               type="text"
               value={this.state.name}
               onChange={this.inputChangeHandler}/>


           <FormElement
               propertyName="year"
               title="Year"
               type="text"
               value={this.state.year}
               onChange={this.inputChangeHandler}
           />
           <FormElement
               propertyName="image"
               title="Image"
               type="file"
               onChange={this.fileChangeHandler}
           />

           <FormGroup row>
               <Col sm={{offset: 2, size: 10}}>
                   <Button type="submit" color="primary">Save</Button>
               </Col>
           </FormGroup>

       </Form>

        );
    }
}


const mapStateToProps = state => ({
    artists: state.reducer.artists
});

const mapDispatchToProps = dispatch => ({
    createAlbum: (albumData) => dispatch(createAlbum(albumData)),
    fetchArtists: () => dispatch(fetchArtists()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddAlbum);