const mongoose = require('mongoose');


const Schema = mongoose.Schema;

const ArtistSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    name: {
        type: String, required: true
    },
    published: {
        type: Boolean,
        required: true,
        default: false
    },
    delete: {
        type: Boolean,
        required:true,
        default: false
    },
    image: String,
    text: String
});

const Artist = mongoose.model('Artist', ArtistSchema);

module.exports = Artist;

