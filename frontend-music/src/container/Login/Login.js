import React, {Component, Fragment} from 'react';
import {Alert, Button, Col, Container, Form, FormGroup} from "reactstrap";
import FormElement from "../../components/UI/Form/FromElement";
import {loginUser} from "../../store/usersActions";
import {connect} from "react-redux";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";


class Login extends Component {

    state = {
        username: '',
        password: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name] : event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.loginUser({...this.state})
    };

    render() {
        return (
            <Fragment>

                <Container>
                    <h2 className="mt-3 mb-3">Login</h2>
                    {this.props.error && (
                        <Alert color="danger">
                            {this.props.error.error || this.props.error.global}
                        </Alert>
                    )}
                    <Form onSubmit={this.submitFormHandler}>
                        <FormGroup>
                            <FacebookLogin/>
                        </FormGroup>
                        <FormElement
                            title="Login"
                            propertyName="username"
                            type="text"
                            value={this.state.username}
                            onChange={this.inputChangeHandler}
                            placeholder="Enter username you registered with"
                            autoComplete="current-username"
                        />

                        <FormElement
                            title="Password"
                            propertyName="password"
                            type="password"
                            value={this.state.password}
                            onChange={this.inputChangeHandler}
                            placeholder="Enter password"
                            autoComplete="current-username"
                        />

                        <FormGroup row>
                            <Col sm={{offset: 2, size: 10}}>
                                <Button type="submit" color="primary">
                                    Login
                                </Button>
                            </Col>
                        </FormGroup>
                    </Form>
                </Container>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.user.registerError
});

const mapDispatchToProps = dispatch => ({
    loginUser: userData => dispatch(loginUser(userData))
});


export default connect(mapStateToProps, mapDispatchToProps)(Login);