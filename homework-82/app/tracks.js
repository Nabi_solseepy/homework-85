const express = require('express');
const Tracks = require('../models/Track');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const tryAuth = require('../middleware/tryAuth');

const router = express.Router();
router.get('/', tryAuth,(req,res) =>{
    let query ;
    if (req.query.album && req.user && req.user.role === 'admin') {
        query = {album: req.query.album, delete: false}
    } else {
        query = {album: req.query.album, published: true, delete: false}
    }

    Tracks.find(query).populate('album').sort({trackNumber: 1})
        .then(tracks => res.send(tracks))
        .catch(() => res.sendStatus(500))

});


router.get('/:id/toggle_published', [auth, permit(['admin'])], async (req, res) => {
    try{
        await Tracks.updateOne({_id: req.params.id}, {$set: {published: true}});
        res.status(200).send();
    }catch (e) {
        res.status(400).send(e);
    }
});


router.post('/',  auth, async (req, res) => {
    try {
        const tracks = await Tracks.find({album: req.body.album});

        const track = new Tracks(req.body);

        track.user = req.user._id;

        track.trackNumber = tracks.length + 1;

        await track.save();
        res.send(track)

    } catch (e) {
        res.sendStatus(500)
    }


});



router.delete('/delete/:id', [auth, permit('admin')], async (req, res) => {
    try {
        await  Tracks.updateOne({_id: req.params.id}, {$set: {delete: true}});
        res.status(200).send({message: 'ok'})
    }catch (e) {
        res.sendStatus(500)
    }




});


module.exports = router;