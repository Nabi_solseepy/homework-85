import React, {Component, Fragment} from 'react';
import {getTrackHistory} from "../../store/trackHistoryActions";
import {connect} from "react-redux";
import {Card, CardBody, CardText, CardTitle} from "reactstrap";

class TrackHistory extends Component {
    componentDidMount() {
        this.props.getTrackHistory()
    }

    render() {
        return (
            <Fragment>
                <h2>Track history</h2>

                {this.props.history && this.props.history.map(history => (
                    <Card key={history._id}>
                        <CardBody>
                            <CardTitle>{history.track.album.artist.name}</CardTitle>
                            <CardText>{history.track.name}</CardText>
                            <p>{history.datetime}</p>
                        </CardBody>
                    </Card>
                ))

                }

            </Fragment>

        );
    }
}

const mapStateToProps = state => ({
    history: state.trackHistoryReducer.trackHistory
});

const mapDispatchToProps = dispatch => ({
    getTrackHistory: () => dispatch(getTrackHistory())
});

export default connect(mapStateToProps, mapDispatchToProps)(TrackHistory);