import {GET_TRACK_HISTORY_SUCCESS} from "./trackHistoryActions";

const initialState = {
    trackHistory: null
};

const trackHistoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_TRACK_HISTORY_SUCCESS:
            return {...state, trackHistory: action.trackHistory};
        default:
            return state
    }
};


export default trackHistoryReducer