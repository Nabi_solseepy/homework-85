const express = require('express');
const nanoid = require('nanoid');
const path = require('path');
const multer = require('multer');
const config =require('../config');


const permit = require('../middleware/permit');
const auth = require('../middleware/auth');
const tryAuth = require('../middleware/tryAuth');


const  Album =require('../models/Album');
const Track = require('../models/Track');

const  router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cd) => {
        cd(null , config.uploadPath);
    },
    filename: (req, file, cd) => {
        cd(null , nanoid() + path.extname(file.originalname));
    }
});
const upload = multer({storage});

router.get('/' , tryAuth, (req,res) =>{
    let query = {published: true};

    if (req.query.artist && req.user && req.user.role === 'admin') {
        query = {artist: req.query.artist, delete: false};
    } else if(req.query.artist) {
        query = {artist: req.query.artist, published: true, delete: false}
    }


    Album.find(query).sort({year: 1}).populate('artist')
        .then(album => res.send(album))
        .catch(() => res.sendStatus(500))

});
router.get('/:id',(req,res) =>{
    Album.findById(req.params.id).populate('artist')
        .then(album => res.send(album))
        .catch(() => res.sendStatus(500))

});


router.post('/', auth, upload.single('image'), (req, res) => {
    const albumData = req.body;

    console.log(albumData);

    if (req.file){
        albumData.image = req.file.filename;
    }

    const album = new Album(albumData);

    album.user = req.user._id;

    album.save()
        .then(result => res.send(result))
        .catch(error => res.sendStatus(400).send(error))
});


router.get('/:id/toggle_published', [auth, permit('admin')], async (req, res) => {
    try{
        await Album.updateOne({_id: req.params.id}, {$set: {published: true}});
        res.status(200).send();
    }catch (e) {
        res.status(400).send(e);
    }
});


router.delete('/delete/:id', [auth, permit('admin')], async (req, res) => {

    try {

        const album =  await Album.findById(req.params.id);

        console.log(album, 'album');

        await Album.updateMany({artist: album.artist._id}, {$set: {delete: true}});

        if (album.length <= 0) {
            res.sendStatus(200)
        }


        await  Track.updateMany({album: album._id}, {$set: {delete: true}});



        res.status(200).send(album)
    } catch (e) {
        res.send(e)
    }

});


module.exports = router;