import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Card, CardBody, CardColumns,} from "reactstrap";
import {Link} from "react-router-dom";
import ArtistsThumbinail from "../../components/ArtistsThumbnail/ArtistsThumbinail";
import {deleteArtist, fetchArtists, publishedArtist,} from "../../store/actions";

class Artists extends Component {

    componentDidMount() {
        this.props.fetchArtists();
    }


    render() {
        return (
            <Fragment>
                <CardColumns>
                    {this.props.artists ?
                        this.props.artists.map(artist => (
                            <Card key={artist._id}>
                                <ArtistsThumbinail image={artist.image}/>

                                <CardBody>
                                    <Link to={`/${artist.name}/${artist._id}`}>{artist.name}</Link>

                                        {this.props.user && this.props.user.role === 'admin' && (
                                            <Fragment>
                                            {artist.published === false  ? (
                                                <span>
                                                      <p>неопубликовано</p>
                                                <Button onClick={() => this.props.publishedArtist(artist._id)} color="primary">Опубликовать</Button>
                                                </span>

                                                ): null}
                                                <Button onClick={() => this.props.deleteArtist(artist._id)} color="danger">Удалить</Button>
                                            </Fragment>
                                        )}



                                </CardBody>

                            </Card>
                        )): null}


                </CardColumns>
            </Fragment>

        );
    }
}


const mapStateToProps = state => ({
    artists: state.reducer.artists,
    user: state.user.user
});


const mapDispatchToProps = dispatch => ({
    fetchArtists: () => dispatch(fetchArtists()),
    publishedArtist: (id) => dispatch(publishedArtist(id)),
    deleteArtist: (id) => dispatch(deleteArtist(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Artists);