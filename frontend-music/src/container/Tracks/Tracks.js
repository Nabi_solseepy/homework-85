import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {deleteTrack, fetchTracks, publishedTrack} from "../../store/actions";
import {ListGroup, ListGroupItem} from "reactstrap";
import {sendTrackHistory} from "../../store/trackHistoryActions";
import Button from "reactstrap/es/Button";

class Tracks extends Component {
    componentDidMount() {
       this.props.fetchTracks(this.props.match.params.id)

    }

    render() {
        console.log(this.props.tracks);
        return (
            <Fragment>
                    {this.props.tracks ?
                        this.props.tracks.map(track => (

                            <ListGroup onClick={() => this.props.sendTrackHistory({track: track._id})} key={track._id} >

                                <ListGroupItem className="mt-3">  <span>{track.trackNumber}</span> {track.name}
                                    <Fragment>
                                    <span className="float-right">
                                        {track.duration}



                                    {this.props.user && this.props.user.role === 'admin' && (
                                        <Fragment>
                                            {track.published === false ? (
                                                <Fragment>
                                                <span>необубликованый</span>
                                                <Button style={{margin: '10px'}} color="info" onClick={() => this.props.publishedTrack(track._id, track.album)}>опубликовать</Button>
                                                </Fragment>

                                                ): null}
                                            <Button onClick={() => this.props.deleteTrack(track._id)} color="danger">удалить</Button>

                                        </Fragment>
                                        )}
                                    </span>
                                    </Fragment>

                                </ListGroupItem>
                            </ListGroup>
                        )) : null
                    }

            </Fragment>


        );
    }
}

const mapStateToProps = state => ({
    tracks: state.reducer.tracks,
    user: state.user.user
});


const mapDispatchToProps = dispatch => ({
    fetchTracks: (id) => dispatch(fetchTracks(id)),
    sendTrackHistory: (trackId) => dispatch(sendTrackHistory(trackId)),
    publishedTrack: (id, albumId) => dispatch(publishedTrack(id, albumId)),
    deleteTrack: (id) => dispatch(deleteTrack(id))
});

export default connect(mapStateToProps,mapDispatchToProps)(Tracks);