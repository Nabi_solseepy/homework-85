import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';

import 'react-notifications/lib/notifications.css';

import axios from './axios-api';

import thunkMiddleware from 'redux-thunk'
import {Provider} from "react-redux";
import {createBrowserHistory} from "history";
import {connectRouter, routerMiddleware, ConnectedRouter} from "connected-react-router";
import {applyMiddleware, compose, createStore, combineReducers} from "redux";

import  reducer from './store/reducer'
import  trackHistoryReducer from './store/trackHistoryReducer';
import userReducer from './store/userReducer'

const history = createBrowserHistory();


const rootReducer = combineReducers({
    router: connectRouter(history),
    reducer: reducer,
    trackHistoryReducer: trackHistoryReducer,
    user: userReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(thunkMiddleware, routerMiddleware(history)))
);


axios.interceptors.request.use(config => {
    try {
        config.headers['Authorization'] = store.getState().user.user.token
    } catch (e) {
        // do nothing user is
    }
    return config
});



const app = (
        <Provider store={store}>
          <ConnectedRouter history={history}>
            <App/>
          </ConnectedRouter>
        </Provider>

);

ReactDOM.render(app, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
