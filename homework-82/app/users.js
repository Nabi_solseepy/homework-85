const express = require('express');
const config = require('../config');
const permit = require('../middleware/permit');
const auth = require('../middleware/auth');
const User = require('../models/User');
const axios = require('axios');

const nanoid = require('nanoid');

const router = express.Router();

router.post('/', async  (req, res) =>{

    const user = new User({
        username: req.body.username,
        password: req.body.password,
        displayName: req.body.displayName,
        avatarImage: req.body.avatarImage
    });

    user.generateToken();

    try {
        await user.save();
        return res.send({token: user.token})
    } catch (error) {
        return res.status(400).send(error)
    }

});


router.post('/sessions', async (req, res) =>{

    const user = await  User.findOne({username: req.body.username});


    if (!user){
        return res.status(400).send({error: 'Username not found'})
    }

    const isMatch = await  user.checkPassword(req.body.password);

    if (!isMatch){
        return res.status(400).send({error: 'Password is wrong'})
    }

    user.generateToken();

   await  user.save();



    return res.send(user)
});



router.delete('/sessions', async (req,res) => {
    const token = req.get('Authorization');
    const success = {message:'Logged out' };
    if (!token){
        return res.send(success)
    }

    const user = await User.findOne({token});
    if (!user){
        return res.send(success)
    }

    user.generateToken();
    await user.save();

    return res.send(success)

});


router.post('/facebookLogin', async (req, res) => {
    const inputToken = req.body.accessToken;
    const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;
    const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

    try {
        const response = await axios.get(debugTokenUrl);

        const responseData = response.data;




        if (responseData.data.error) {
            return res.status(500).send({error: 'Token incorrect'});
        }

        if (responseData.data.user_id !== req.body.id) {
            return res.status(500).send({error: 'User is wrong'});
        }

        let user = await User.findOne({facebookId: req.body.id});

        if (!user) {
            user = new User({
                username: req.body.email || req.body.id,
                password: nanoid(),
                displayName: req.body.name,
                avatarImage: req.body.picture.data.url ,
                facebookId: req.body.id
            });
        }

        user.generateToken();

        await user.save();

        return res.send(user);
    } catch (e) {
        console.log(e);
        return res.status(500).send({error: 'Something went wrong'});
    }
});


module.exports = router;