import React, {Component, Fragment} from 'react';
import {Alert, Button, Col, Container, Form, FormGroup} from "reactstrap";
import FormElement from "../../components/UI/Form/FromElement";
import {connect} from "react-redux";
import {registerUser} from "../../store/usersActions";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

class Register extends Component {
    state = {
        username: '',
        password: '',
        displayName: '',
        avatarImage: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name] : event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.registerUser({...this.state})
    };


    getFieldError = fieldName  => {
        return this.props.error && this.props.error.errors && this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message
    };

    render() {
        return (
            <Fragment>
                <Container>
                    <h2>Register</h2>
                    {this.props.error && (
                        <Alert color="danger">
                            {this.props.error || this.props.error.global}
                        </Alert>
                    )}
                    <Form onSubmit={this.submitFormHandler}>
                        <FormElement
                            title="Login"
                            propertyName="username"
                            type="text"
                            value={this.state.username}
                            onChange={this.inputChangeHandler}
                            placeholder="Enter username you registered with"
                            error={this.getFieldError('username')}
                            autoComplete="current-username"
                        />

                        <FormElement
                            title="Password"
                            propertyName="password"
                            type="password"
                            value={this.state.password}
                            onChange={this.inputChangeHandler}
                            placeholder="Enter password"
                            error={this.getFieldError('password')}
                            autoComplete="current-password"
                        />

                        <FormElement
                            title="displayName"
                            propertyName="displayName"
                            type="text"
                            value={this.state.displayName}
                            onChange={this.inputChangeHandler}
                            placeholder="Enter displayName"
                            error={this.getFieldError('displayName')}
                            autoComplete="current-displayName"
                        />
                        <FormElement
                            title="avatarImage"
                            propertyName="avatarImage"
                            type="text"
                            value={this.state.avatarImage}
                            onChange={this.inputChangeHandler}
                            placeholder="Enter avatarImage"
                            error={this.getFieldError('avatarImage')}
                            autoComplete="current-avatarImage"
                        />
                        <FormGroup>
                            <FacebookLogin/>
                        </FormGroup>

                        <FormGroup row>
                            <Col sm={{offset: 2, size: 10}}>
                                <Button type="submit" color="primary">
                                    Login
                                </Button>
                            </Col>
                        </FormGroup>
                    </Form>
                </Container>
            </Fragment>
        );
    }
}


const mapStateToProps = state => ({
    error: state.user.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);