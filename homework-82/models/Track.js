const mongoose = require('mongoose');


const Schema = mongoose.Schema;

const TrackSchema = new Schema({
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true
    },
    name: {
        type: String,
        required: true
    },
    album: {
        type: Schema.Types.ObjectId,
        ref: 'Album',
        required: true
    },
    delete: {
        type: Boolean,
        required:true,
        default: false
    },
    published: {
        type: Boolean,
        required: true,
        default: false
    },
    duration: String,
    trackNumber: {type: Number}
});

const Track = mongoose.model('Track', TrackSchema );


module.exports = Track;