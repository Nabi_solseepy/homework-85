import axios from '../axios-api'
import {push} from 'connected-react-router'
export  const FETCH_ALBUMS_SUCCESS = ' FETCH_ALBUMS_SUCCESS';
export  const FETCH_ARTISTS_SUCCESS = ' FETCH_ARTISTS_SUCCESS';
export const FETCH_TRECKS_SUCCESS = 'FETCH_TRECKS_SUCCESS';


export const CREATE_TRACK_SUCCESS = 'CREATE_TRACK_SUCCESS ';

export const CREATE_ARTIST_SUCCESS = 'CREATE_ARTIST_SUCCESS';

export const CREATE_ALBUM_SUCCESS = 'CREATE_ALBUM_SUCCESS';

export const PUBLISHED_ARTIST_SUCCESS = 'PUBLISHED_ARTIST_SUCCESS';

export const PUBLISHED_ALBUM_SUCCESS = 'PUBLISHED_ALBUM_SUCCESS';
export const PUBLISHED_TRECK_SUCCESS = 'PUBLISHED_TRECK_SUCCESS';


export const DELETE_ALBUM_SUCCESS = 'DELETE_ALBUM_SUCCESS';



export const DELETE_ARTISTS_SUCCESS = 'DELETE_ARTISTS_SUCCESS';


export const DELETE_TRACK_SUCCESS = 'DELETE_TRACK_SUCCESS';

export const deleteTrackSuccess = () => ({type: DELETE_TRACK_SUCCESS});

export const deleteAlbumSuccess = () => ({type: DELETE_ALBUM_SUCCESS});


export const createTrackSuccess = () => ({type: CREATE_TRACK_SUCCESS});

export const publishedAlbumSuccess = () => ({type: PUBLISHED_ALBUM_SUCCESS});

export const publishedArtistSuccess = () => ({type: PUBLISHED_ARTIST_SUCCESS});

export const publishedTreckSuccess = () => ({type: PUBLISHED_TRECK_SUCCESS});

export const fetchAlbumsSuccess = (albums) => ({type: FETCH_ALBUMS_SUCCESS, albums});
export const fetchArtistsSuccess = (artists) => ({type: FETCH_ARTISTS_SUCCESS, artists});
export const fetchTrecksSuccess = (track) => ({type: FETCH_TRECKS_SUCCESS, track});

export const createArtistSuccess = () => ({type: CREATE_ARTIST_SUCCESS});

export const createAlbumSuccess = () => ({type: CREATE_ALBUM_SUCCESS});

export const deleteArtistsSuccess = () => ({type: DELETE_ARTISTS_SUCCESS});





export const fetchAlbums = (id) => {
    return dispatch => {
        let url = "/albums";
        if (id) {
            url += `?artist=${id}`
        }
        axios.get(url).then(response => {
            dispatch(fetchAlbumsSuccess(response.data))
        })
    }
};

export const fetchArtists = () => {
    return dispatch => {
        axios.get('/artists').then(response => {

            dispatch(fetchArtistsSuccess(response.data))
        })
    }
};

export const fetchTracks = (id) => {
    return dispatch => {
        axios.get('/tracks?album=' + id).then(response => {
            dispatch(fetchTrecksSuccess(response.data))
        })
    }
};

export const createArtist = (artistData) => {
    return dispatch => {
        axios.post('/artists', artistData).then(
            () => {
                dispatch(createArtistSuccess());
                dispatch(push('/'))

            }
        )
    }
};


export const publishedArtist = (id) => {
    console.log(id);
    return dispatch => {
        axios.get(`/artists/${id}/toggle_published`).then(
            () => {
                dispatch(publishedArtistSuccess());
                dispatch(fetchArtists())
            }
        )
    }
};

export const publishedAlbum = (id) => {
    return dispatch => {
        axios.get(`/albums/${id}/toggle_published`).then(
            () => {
                dispatch(publishedAlbumSuccess());
                dispatch(fetchAlbums(id))
            }
        )
    }
};

export const publishedTrack = (id, albumId) => {
    return dispatch  =>{
        axios.get(`/tracks/${id}/toggle_published`).then(
            () => {
                dispatch(publishedTreckSuccess());
                dispatch(fetchTracks(albumId))
            }
        )
    }
};

export const createAlbum = (albumData) => {
    return dispatch => {
       axios.post('/albums', albumData).then(
           () => {
               dispatch(createAlbumSuccess());
               dispatch(push('/'))
           }
       )
    }
};


export const createTrack = (trackData) => {
    return dispatch => {
        axios.post('/tracks', trackData).then(
            () => {
                dispatch(createTrackSuccess());
                dispatch(push('/'))
            }
        )
    }
};

export const deleteArtist = (artistId) => {
    console.log(artistId);
    return dispatch => {
        axios.delete('/artists/delete/' + artistId).then(
            () => {dispatch(deleteArtistsSuccess());
                dispatch(fetchArtists())}
        )
    }
};


export const delteAlbums = (albumId, artistId) => {
    return dispatch => {
        axios.delete('/albums/delete/' + albumId).then(
            dispatch(deleteAlbumSuccess()),
            dispatch(fetchAlbums(artistId))
        )
    }
};


export const deleteTrack = (id) => {
    return dispatch => {
        axios.delete('/tracks/delete/' + id).then(
            dispatch(deleteTrackSuccess()),
            dispatch(fetchTracks(id))
        )
    }
};