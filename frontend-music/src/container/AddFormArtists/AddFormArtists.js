import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "reactstrap";
import {connect} from "react-redux";
import {createArtist} from "../../store/actions";
import FormElement from "../../components/UI/Form/FromElement";

class AddFormArtists extends Component {
    state = {
        name: '',
        image: '',
        text: ''
    };


    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.createArtist(formData);
    };



    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    render() {
        return (

            <Form onSubmit={this.submitFormHandler}>
                <h2>Add Artists</h2>
                <FormElement
                    propertyName="name"
                    title="Name"
                    type="text"
                    value={this.state.name}
                    onChange={this.inputChangeHandler}/>
                <FormElement
                    propertyName="text"
                    title="Text"
                    type="textarea"
                    onChange={this.inputChangeHandler}
                />
                <FormElement
                    propertyName="image"
                    title="Image"
                    type="file"
                    onChange={this.fileChangeHandler}
                />
                <FormGroup row>
                    <Col sm={{offset: 2, size: 10}}>
                        <Button type="submit" color="primary">Save</Button>
                    </Col>
                </FormGroup>

            </Form>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createArtist: (artistData) => dispatch(createArtist(artistData))
});

export default connect(null, mapDispatchToProps)(AddFormArtists);