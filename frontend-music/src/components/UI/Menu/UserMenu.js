import React,{Fragment} from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, NavLink, UncontrolledDropdown} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

const UserMenu = ({user,logout}) => {
    return (
    <Fragment>
        <UncontrolledDropdown nav inNavbar>
            <DropdownToggle nav caret>
                Hello, {user.displayName}
                <img src={user.avatarImage} style={{paddingLeft: '10px', height: '30px'}} alt="avatar"/>
            </DropdownToggle>
            <DropdownMenu right>

                    <NavLink tag={RouterNavLink} to="/track_history" >Track History</NavLink>
                <DropdownItem>

                    <NavLink tag={RouterNavLink} to="/addAlbum" >Add Album</NavLink>

                </DropdownItem>
                <DropdownItem>
                    <NavLink tag={RouterNavLink} to="/addArtists" >Add Artists</NavLink>
                </DropdownItem>
                <DropdownItem>
                    <NavLink tag={RouterNavLink} to="/addTracks" >Add Tracks</NavLink>
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem onClick={logout}>
                    Logout
                </DropdownItem>
            </DropdownMenu>
        </UncontrolledDropdown>
    </Fragment>
    );
};

export default UserMenu;