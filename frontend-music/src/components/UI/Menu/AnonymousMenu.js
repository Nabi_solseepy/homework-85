import React, {Fragment} from 'react';
import { NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

const AnonymousMenu = (props) => {
    return (
        <Fragment>
            <NavItem>
                <NavLink tag={RouterNavLink} to="/" exact>Artists</NavLink>
            </NavItem>
            {props.user ? (
                <NavItem>
                    <NavLink tag={RouterNavLink} to="/track_history" >Track History</NavLink>
                </NavItem>
            ):(
                <Fragment>
                    <NavItem>
                        <NavLink tag={RouterNavLink} to="/register" >Sign up</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink tag={RouterNavLink} to="/login" >Login</NavLink>
                    </NavItem>


                </Fragment>
            )}
        </Fragment>
    );
};

export default AnonymousMenu;