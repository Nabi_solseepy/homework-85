const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    dbUrl: 'mongodb://localhost/music',
    mongoOptions: {
        useNewUrlParser: true,
        useCreateIndex: true
        },
    facebook: {
        appId: '2298426760415006',
        appSecret: 'e2946462d9941c1f059f40eed56dc92d' // insecure!
    }

};
