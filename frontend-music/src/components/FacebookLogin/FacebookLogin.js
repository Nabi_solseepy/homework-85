import React, {Component} from 'react';
import {NotificationManager} from 'react-notifications'
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props'
import {Button} from "reactstrap";
import {facebookLogin} from "../../store/usersActions";
import {connect} from "react-redux";



class FacebookLogin extends Component {
    facebookLogin = data => {
        if (data.error){
            NotificationManager.error('Something went wrong!');
            this.props.facebookLogin(data)
        } else if (!data.name) {
            NotificationManager.error('You pressed cancel');
        } else {
            this.props.facebookLogin(data);
        }
    };


    render() {
        return (
            <FacebookLoginButton
                appId="2298426760415006"
                callback={this.facebookLogin}
                fields="name,email,picture"
                render={renderProps => (
                    <Button onClick={renderProps.onClick} color="primary">Login with Facebook</Button>
                )}
            />
        );
    }
}


const mapDispatchToProps = dispatch => ({
    facebookLogin: userData => dispatch(facebookLogin(userData))
});



export default connect(null, mapDispatchToProps)(FacebookLogin);