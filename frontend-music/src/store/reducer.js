import {FETCH_ALBUMS_SUCCESS, FETCH_ARTISTS_SUCCESS, FETCH_TRECKS_SUCCESS} from "./actions";

const initialState = {
    artists: [],
    albums: null,
    tracks: null,
    album: null
};



const reducer = (state = initialState, action) => {

    switch (action.type) {
        case FETCH_ARTISTS_SUCCESS:
            return {...state, artists: action.artists};
        case FETCH_ALBUMS_SUCCESS:
            return {
                ...state,
                albums: action.albums
            };
        case FETCH_TRECKS_SUCCESS:
            return {...state, tracks: action.track};
        default:
            return state

    }
};

export default reducer;